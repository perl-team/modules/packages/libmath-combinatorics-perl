libmath-combinatorics-perl (0.09-6) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.1.5, no changes needed.
  * Apply multi-arch hints. + libmath-combinatorics-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 04 Dec 2022 19:21:16 +0000

libmath-combinatorics-perl (0.09-5.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 04 Jan 2021 17:39:01 +0100

libmath-combinatorics-perl (0.09-5) unstable; urgency=medium

  * Team upload

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ Nuno Carvalho ]
  * debian/control: remove Gustavo Franco from Uploaders list
    (Closes: #729422).

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Carlo Segre from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Florian Schlichting ]
  * Add spelling.patch
  * Bump dh compat to level 11
  * Declare compliance with Debian Policy 4.1.4

 -- Florian Schlichting <fsfs@debian.org>  Sun, 01 Jul 2018 23:02:00 +0200

libmath-combinatorics-perl (0.09-4) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control:
    - add Vcs-Svn field (source stanza); Vcs-Browser field (source stanza);
      Homepage field (source stanza)
    - remove XS-Vcs-Svn fields
    - add ${misc:Depends} to Depends: field
    - improve short and long description
  * Switch to source format 3.0 (quilt).
  * Minimize debian/rules.
  * Don't install README anymore.
  * Email change: gregor herrmann -> gregoa@debian.org
  * Re-upload, this time really with 0.09 (the last upload had the 0.09
    .orig.tar.gz but the contents of 0.08); closes: #616509.
  * debian/watch: use dist-based URL.
  * Switch to debhelper 8.
  * Set Standards-Version to 3.9.1 (no changes).
  * Machine-readable debian/copyright.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Joachim Breitner ]
  * Removed myself from uploaders.

  [ Russ Allbery ]
  * Remove myself from Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Sat, 05 Mar 2011 14:56:15 +0100

libmath-combinatorics-perl (0.09-3) unstable; urgency=low

  * Add dh_md5sums to debian/rules.
  * Don't ignore errors of make clean.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Fri, 17 Aug 2007 13:07:13 +0200

libmath-combinatorics-perl (0.09-2) unstable; urgency=low

  * Updated to compatibility level 5.

 -- Carlo Segre <segre@debian.org>  Fri, 29 Dec 2006 02:15:04 -0600

libmath-combinatorics-perl (0.09-1) unstable; urgency=low

  * New upstream release

 -- Carlo Segre <segre@debian.org>  Sat, 16 Dec 2006 10:21:46 -0600

libmath-combinatorics-perl (0.08-2) unstable; urgency=low

  * Update standards version to 3.7.2
  * Fix Build-Depends-Indep lintian bug

 -- Carlo Segre <segre@iit.edu>  Thu,  8 Jun 2006 13:21:47 -0500

libmath-combinatorics-perl (0.08-1) unstable; urgency=low

  * New upstream release

 -- Carlo Segre <segre@iit.edu>  Thu,  1 Dec 2005 12:01:19 -0600

libmath-combinatorics-perl (0.07-1) unstable; urgency=low

  * New upstream release

 -- Carlo Segre <segre@iit.edu>  Sun, 13 Nov 2005 18:51:17 -0600

libmath-combinatorics-perl (0.06-1) unstable; urgency=low

  * New upstream release (Closes: #329586)

 -- Carlo Segre <segre@iit.edu>  Tue, 27 Sep 2005 09:42:17 -0500

libmath-combinatorics-perl (0.04-1) unstable; urgency=low

  * Initial Release (Closes: #309574)
  * Maintainer -
    Debian Perl Group <pkg-perl-maintainer@lists.alioth.debian.org>
    via Carlo Segre <segre@iit.edu>

 -- Carlo Segre <segre@iit.edu>  Mon,  2 May 2005 14:45:06 -0500
