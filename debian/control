Source: libmath-combinatorics-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl
Standards-Version: 4.1.5
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libmath-combinatorics-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libmath-combinatorics-perl.git
Homepage: https://metacpan.org/release/Math-Combinatorics

Package: libmath-combinatorics-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends}
Multi-Arch: foreign
Description: module for performing combinations and permutations on lists
 Combinatorics is the branch of mathematics studying the enumeration,
 combination, and permutation of sets of elements and the mathematical
 relations that characterize their properties. As a jumping off point,
 refer to:
 .
 http://mathworld.wolfram.com/Combinatorics.html
 .
 Math::Combinatorics provides a pure-perl implementation of nCk, nPk, and n!
 (combination, permutation, and factorial, respectively).
